import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalcTest {

    private Calc calc;

    @Before
    public void setup() {
        calc = new Calc();
    }

    @After
    public void teardown(){
        calc = null;
    }

    @Test
    public void testAdd_addPositiveNumbers_shouldBeAdded(){
        int x = 6, y = 9;
        Assert.assertEquals(x+y, calc.add(x, y));
    }
}
